const express = require("express");
const router = express.Router();
const prodController = require("../controller/prodController.js");
const auth = require("../auth.js");

router.post("/create", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		prodController.addProduct(data.product).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

router.get("/active", (req, res) => {
	prodController.availableProducts().then(resultFromController => res.send(resultFromController));
})

router.get("/:prodId", (req, res) => {
	prodController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/update/:prodId", auth.verify, (req, res) => {
	const data = {
		prod: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {
		prodController.updateProduct(data.params, data.prod).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

router.patch("/archive/:prodId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if (data.isAdmin) {
		prodController.archiveProduct(data.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

})

module.exports = router;