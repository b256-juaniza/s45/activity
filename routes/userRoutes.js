const express = require("express");
const router = express.Router();
const userController = require("../controller/userController.js");
const auth = require("../auth.js");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/allUsers", (req, res) => {
	userController.allUsers().then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

})

router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		prodId : req.body.prod,
		quantity : req.body.quantity,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		email : auth.decode(req.headers.authorization).email,
		userId : auth.decode(req.headers.authorization).id
	}
	if (data.isAdmin == false) {
		userController.checkout(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

router.put("/admin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin) {
		userController.setAdmin(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

router.get("/order/:userId", (req, res) => {
	userController.getOrder(req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router;