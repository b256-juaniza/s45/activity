const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js");

module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})
	return newUser.save().then((user, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.allUsers = () => {
	return User.find({}).then(result => {
		let index = 0;
		result.forEach(i => {
			result[index].password = "HIDDEN";
			index++;
		})
		return result;
	})
}

module.exports.authenticateUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.checkout = async (reqbody) => {
	let price;
	let name = await Product.findById(reqbody.prodId).then(result => {
		let increment;
		price = result.price;
		if (result.userOrdered.length == 0) {
			increment = 1;
		} else {
			increment = result.userOrdered.length + 1;
		}

		let userOrder = { 
				userId: reqbody.userId,
				orderId: increment,
				email : reqbody.email
			}

		result.userOrdered.push(userOrder);
		result.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
		return result.name;
	});

	let order = {
		products : [{
			productId : reqbody.prodId,
			productName : name,
			quantity : reqbody.quantity
		}],
		totalAmount : reqbody.quantity * price,
	}

	return await User.findById(reqbody.userId).then(result => {
		result.orderedProduct.push(order);
		return result.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
	})
};

module.exports.getOrder = (paramsId) => {
	let order = User.findById(paramsId.userId).then(result => {
		let userOrder = {
			Email : result.email,
			Orders : []
		}
		let orders = result.orderedProduct.forEach(i => {
			let hours = i.purchaseOn.getHours();
			let midday = hours >= 12 ? 'pm' : 'am';
			hours = (hours % 12) || 12;
			let month = i.purchaseOn.getMonth()+1;
			let day = i.purchaseOn.getUTCDate();
			let year = i.purchaseOn.getUTCFullYear();
			let date = month + "-" + day + "-" + year;
			let time = hours + ":" + i.purchaseOn.getMinutes() + " " + midday;
			let prod = {
				Product : i.products[0].productName,
				quantity : i.products[0].quantity,
				Total : i.totalAmount,
				Purchase_Date : date,
				Purchase_Time : time
			}
			userOrder.Orders.push(prod);
		})
		if (result.isAdmin) {
			return false;
		} else {
			return userOrder;
		}
	})
	if (order == false) {
		return false;
	} else {
		return order;
	}
}

module.exports.setAdmin = (reqbody) => {
	return User.findOneAndUpdate(reqbody, {isAdmin: true}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}
